use mesh_rs_sdk_main::WASM_LOG_ENV_NAME;

use std::io::Write;
use std::env::var;

const IT_MODULE_PATH: &str = "wasmer_interface_types_fl";
const RUST_LOG_ENV_NAME: &str = "RUST_LOG";

pub(super) fn init_logger() {
    use log::LevelFilter::Info;

    match (var(RUST_LOG_ENV_NAME), var(WASM_LOG_ENV_NAME)) {
        (Ok(_), _) => {}
        (Err(_), Ok(wasm_log_env)) if !wasm_log_env.starts_with("off") => {
            std::env::set_var(RUST_LOG_ENV_NAME, "trace")
        }
        _ => return,
    };

    env_logger::builder()
        .format(|buf, record| {
            match record.module_path() {
                Some(module_path) if module_path.starts_with(IT_MODULE_PATH) => {
                    writeln!(buf, "[host] {}", record.args())
                }
                // due to the log_utf8_string implementation,
                // a log message from a Wasm module always has module path
                None => writeln!(buf, "[host] {}", record.args()),
                Some(module_path) => writeln!(buf, "[{}] {}", module_path, record.args()),
            }
        })
        // set a default level Info for Wasmer components
        .filter(Some("cranelift_codegen"), Info)
        .filter(Some("wasmer_wasi"), Info)
        .filter(Some("wasmtime_wasi"), Info)
        //.filter(Some(WIT_MODULE_PATH), Info)
        // the same for rustyline and mesh
        .filter(Some("rustyline"), Info)
        .filter(Some("mesh"), Info)
        .init();
}
