use mesh_wasm_backend_traits::WasiState;

pub(super) fn print_envs(module_name: &str, wasi_state: &dyn WasiState) {
    let envs = wasi_state.envs();
    if envs.is_empty() {
        println!("{} don't have environment variables", module_name);
        return;
    }

    println!("Environment variables:");
    for env in envs.iter() {
        match String::from_utf8(env.clone()) {
            Ok(string) => println!("{}", string),
            Err(_) => println!("{:?}", env),
        }
    }
}

pub(super) fn print_fs_state(_wasi_state: &dyn WasiState) {
    println!("Printing WASI filesystem state is not supported now.");
}
