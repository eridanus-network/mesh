use crate::cargo_manifest::ManifestError;

use mesh_module_info_parser::ModuleInfoError;
use mesh_it_generator::ITGeneratorError;
use mesh_it_parser::ITParserError;

use thiserror::Error as ThisError;

use std::path::PathBuf;

#[derive(Debug, ThisError)]
pub enum CLIError {
    /// Unknown command was entered by user.
    #[error("{0} is an unknown command")]
    NoSuchCommand(String),

    /// A error occurred while embedding rust sdk version.
    #[error(transparent)]
    VersionEmbeddingError(#[from] ModuleInfoError),

    /// An error occurred while generating interface types.
    #[error(transparent)]
    ITGeneratorError(#[from] ITGeneratorError),

    /// An error occurred while parsing interface types.
    #[error(transparent)]
    ITParserError(#[from] ITParserError),

    /// An error occurred when no Wasm file was compiled.
    #[error("{0}")]
    WasmCompilationError(String),

    /// Various errors related to I/O operations.
    #[error("{0:?}")]
    IOError(#[from] std::io::Error),

    #[error(transparent)]
    ManifestError(#[from] ManifestError),

    #[error("Error loading lockfile at {0}: {1}")]
    LockfileError(PathBuf, cargo_lock::Error),

    #[error(transparent)]
    MetadataError(#[from] cargo_metadata::Error),
}
