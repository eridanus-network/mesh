use std::process::Command;

const MESH_TEMPLATE_URL: &str = "https://gitee.com/eridanus-network/mesh-template";

pub(crate) fn generate(
    project_name: Option<&str>,
    should_be_initialized: bool,
) -> Result<(), anyhow::Error> {
    if !is_cargo_generate_installed()? {
        println!(
            "to use generate `cargo-generate` should be installed:\ncargo install cargo-generate"
        );
        return Ok(());
    }

    let mut cargo = Command::new("cargo");
    cargo.arg("generate").arg("--git").arg(MESH_TEMPLATE_URL);

    if let Some(project_name) = project_name {
        cargo.arg("--name").arg(project_name);
    }
    if should_be_initialized {
        cargo.arg("--init");
    }

    crate::utils::run_command_inherited(cargo).map(|_| ())
}

fn is_cargo_generate_installed() -> Result<bool, anyhow::Error> {
    let mut cargo = Command::new("cargo");
    cargo.arg("install").arg("--list");

    let output = crate::utils::run_command_piped(cargo)?;
    Ok(output.contains("cargo-generate"))
}
