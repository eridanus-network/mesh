use thiserror::Error;

//TODO: use enum with strict errors instead of String
#[derive(Debug, Error)]
pub enum ITJsonSeDeError {
    #[error("{0}")]
    Se(String),

    #[error("{0}")]
    De(String),
}
