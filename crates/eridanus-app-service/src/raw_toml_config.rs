use crate::config::AppServiceConfig;
use crate::AppServiceError;
use crate::Result;

use mesh::TomlMeshConfig;

use serde_derive::Deserialize;
use serde_derive::Serialize;

use std::convert::TryInto;
use std::path::PathBuf;

#[derive(Deserialize, Serialize, Debug, Clone, Default)]
pub struct TomlAppServiceConfig {
    pub service_working_dir: Option<String>,
    pub service_base_dir: Option<String>,

    #[serde(flatten)]
    pub toml_mesh_config: TomlMeshConfig,
}

impl TomlAppServiceConfig {
    /// Load config from filesystem.
    pub fn load<P: Into<PathBuf>>(path: P) -> Result<Self> {
        let path = path.into();
        let file_content = std::fs::read(&path)?;
        toml::from_slice(&file_content).map_err(|e| {
            AppServiceError::ConfigParseError(format!("Error parsing config {:?}: {:?}", path, e))
        })
    }
}

impl TryInto<AppServiceConfig> for TomlAppServiceConfig {
    type Error = AppServiceError;

    fn try_into(self) -> Result<AppServiceConfig> {
        let mesh_config = self.toml_mesh_config.try_into()?;
        let service_working_dir = match self.service_working_dir {
            Some(service_base_dir) => PathBuf::from(service_base_dir),
            // use tmp dir for service base dir if it isn't defined
            None => std::env::temp_dir(),
        };

        let service_tmp_dir = match self.service_base_dir {
            Some(tmp_dir) => PathBuf::from(tmp_dir),
            None => service_working_dir.clone(),
        };

        Ok(AppServiceConfig {
            service_working_dir,
            service_base_dir: service_tmp_dir,
            mesh_config,
        })
    }
}
