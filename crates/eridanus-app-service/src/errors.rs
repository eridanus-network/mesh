use mesh::MeshError;

use std::error::Error;
use std::io::Error as IOError;
use std::path::PathBuf;

#[derive(Debug)]
pub enum AppServiceError {
    /// An error related to config parsing.
    InvalidConfig(String),

    /// Various errors related to file i/o.
    IOError(IOError),

    /// Mesh errors.
    MeshError(MeshError),

    /// Directory creation failed
    CreateDir { err: IOError, path: PathBuf },

    /// Errors related to malformed config.
    ConfigParseError(String),
}

impl Error for AppServiceError {}

impl std::fmt::Display for AppServiceError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            AppServiceError::InvalidConfig(err_msg) => write!(f, "{}", err_msg),
            AppServiceError::IOError(err) => write!(f, "{}", err),
            AppServiceError::MeshError(err) => write!(f, "{}", err),
            AppServiceError::CreateDir { err, path } => {
                write!(f, "Failed to create dir {:?}: {:?}", path, err)
            }
            AppServiceError::ConfigParseError(err_msg) => write!(f, "{}", err_msg),
        }
    }
}

impl From<MeshError> for AppServiceError {
    fn from(err: MeshError) -> Self {
        AppServiceError::MeshError(err)
    }
}

impl From<IOError> for AppServiceError {
    fn from(err: IOError) -> Self {
        AppServiceError::IOError(err)
    }
}

impl From<toml::de::Error> for AppServiceError {
    fn from(err: toml::de::Error) -> Self {
        AppServiceError::InvalidConfig(format!("{}", err))
    }
}

impl From<std::convert::Infallible> for AppServiceError {
    fn from(_: std::convert::Infallible) -> Self {
        unreachable!()
    }
}
