#![warn(rust_2018_idioms)]
#![deny(
    dead_code,
    nonstandard_style,
    unused_imports,
    unused_mut,
    unused_variables,
    unused_unsafe,
    unreachable_patterns
)]

mod config;
mod errors;
mod service;
mod service_interface;
mod raw_toml_config;

pub(crate) type Result<T> = std::result::Result<T, AppServiceError>;

pub use errors::AppServiceError;
pub use service::AppService;
pub use service_interface::FunctionSignature;
pub use service_interface::RecordType;
pub use service_interface::ServiceInterface;

pub use config::AppServiceConfig;
pub use raw_toml_config::TomlAppServiceConfig;

pub use mesh::ConfigContext;
pub use mesh::WithContext;
pub use mesh::MeshConfig;
pub use mesh::MeshModuleConfig;
pub use mesh::MeshWASIConfig;
pub use mesh::TomlMeshConfig;
pub use mesh::TomlMeshModuleConfig;
pub use mesh::TomlMeshNamedModuleConfig;
pub use mesh::TomlWASIConfig;
pub use mesh::ModuleDescriptor;

pub use mesh::MeshError;

pub use mesh::IValue;
pub use mesh::IRecordType;
pub use mesh::IFunctionArg;
pub use mesh::IType;
pub use mesh::HostImportDescriptor;
pub use mesh::HostImportError;
pub use mesh::to_interface_value;
pub use mesh::from_interface_values;
pub use mesh::ModuleMemoryStat;
pub use mesh::MemoryStats;
pub use mesh::ne_vec;

pub use mesh_min_it_version::min_sdk_version;
pub use mesh_min_it_version::min_it_version;

pub use mesh::CallParameters;
pub use mesh::SecurityTetraplet;
