use mesh::MeshConfig;

use std::path::PathBuf;

/// Describes behaviour of the Eridanus AppService.
#[derive(Default)]
pub struct AppServiceConfig {
    /// Used for preparing filesystem on the service initialization stage.
    pub service_working_dir: PathBuf,
    /// Location for /tmp and /local dirs.
    pub service_base_dir: PathBuf,
    pub mesh_config: MeshConfig,
}
