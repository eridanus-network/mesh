use mesh::itype_text_view;
use mesh::IRecordType;
use mesh::MRecordTypes;
use mesh::MeshFunctionSignature;
use mesh::MeshModuleInterface;

use serde::Serialize;

use std::sync::Arc;

#[derive(Serialize)]
pub struct FunctionSignature {
    pub name: String,
    pub arguments: Vec<(String, String)>,
    pub output_types: Vec<String>,
}

#[derive(Serialize)]
pub struct RecordType {
    pub name: String,
    pub id: u64,
    pub fields: Vec<(String, String)>,
}

#[derive(Serialize)]
pub struct ServiceInterface {
    pub function_signatures: Vec<FunctionSignature>,
    pub record_types: Vec<RecordType>,
}

pub(crate) fn into_service_interface(mesh_interface: MeshModuleInterface<'_>) -> ServiceInterface {
    let record_types = mesh_interface.record_types;

    let function_signatures = mesh_interface
        .function_signatures
        .into_iter()
        .map(|sign| serialize_function_signature(sign, record_types))
        .collect();

    let record_types = record_types
        .iter()
        .map(|(id, record)| serialize_record_type(*id, record.clone(), record_types))
        .collect::<Vec<_>>();

    ServiceInterface {
        function_signatures,
        record_types,
    }
}

fn serialize_function_signature(
    signature: MeshFunctionSignature,
    record_types: &MRecordTypes,
) -> FunctionSignature {
    let arguments = signature
        .arguments
        .iter()
        .map(|arg| (arg.name.clone(), itype_text_view(&arg.ty, record_types)))
        .collect();

    let output_types = signature
        .outputs
        .iter()
        .map(|itype| itype_text_view(itype, record_types))
        .collect();

    FunctionSignature {
        name: signature.name.to_string(),
        arguments,
        output_types,
    }
}

fn serialize_record_type(
    id: u64,
    record: Arc<IRecordType>,
    record_types: &MRecordTypes,
) -> RecordType {
    let fields = record
        .fields
        .iter()
        .map(|field| (field.name.clone(), itype_text_view(&field.ty, record_types)))
        .collect::<Vec<_>>();

    RecordType {
        name: record.name.clone(),
        id,
        fields,
    }
}
