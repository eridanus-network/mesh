use crate::it_interface::ITInterfaceError;
use thiserror::Error as ThisError;

#[derive(Debug, ThisError)]
pub enum InterfaceError {
    #[error("record type with type id {0} not found")]
    NotFoundRecordTypeId(u64),

    #[error(transparent)]
    ITInterfaceError(#[from] ITInterfaceError),
}
