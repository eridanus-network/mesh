mod errors;
mod interface_transformer;
mod itype_to_text;
mod module_interface;
mod records_transformer;

pub use errors::InterfaceError;
pub use interface_transformer::it_to_module_interface;
pub use itype_to_text::*;
pub use module_interface::*;

pub type InterfaceResult<T> = std::result::Result<T, InterfaceError>;

use mesh_it_interfaces::MITInterfaces;

/// Returns interface of a Mesh module.
pub fn get_interface(mit: &MITInterfaces<'_>) -> InterfaceResult<ModuleInterface> {
    let it_interface = crate::it_interface::get_interface(mit)?;
    let interface = it_to_module_interface(it_interface)?;

    Ok(interface)
}
