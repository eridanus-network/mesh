use wasmer_it::IType;
use wasmer_it::ast::FunctionArg as IFunctionArg;
use wasmer_it::IRecordType;

use serde::Serialize;
use serde::Deserialize;

use std::collections::HashMap;
use std::sync::Arc;

pub type IRecordTypes = HashMap<u64, Arc<IRecordType>>;

/// Represent a function type inside Mesh module.
#[derive(PartialEq, Eq, Debug, Clone, Hash, Serialize, Deserialize)]
pub struct IFunctionSignature {
    pub name: Arc<String>,
    pub arguments: Arc<Vec<IFunctionArg>>,
    pub outputs: Arc<Vec<IType>>,
    pub adapter_function_type: u32,
}

/// Represent an interface of a Wasm module.
#[derive(PartialEq, Eq, Debug, Clone, Serialize, Deserialize)]
pub struct IModuleInterface {
    pub export_record_types: IRecordTypes,
    pub record_types: IRecordTypes,
    pub function_signatures: Vec<IFunctionSignature>,
}
