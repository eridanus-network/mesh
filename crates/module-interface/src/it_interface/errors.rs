use mesh_it_interfaces::MITInterfacesError;
use thiserror::Error as ThisError;

#[derive(Debug, ThisError)]
pub enum ITInterfaceError {
    #[error("type with idx = {0} isn't a function type")]
    ITTypeNotFunction(u32),

    #[error("record type with type id {0} not found")]
    NotFoundRecordTypeId(u64),

    #[error("mailformed module: a record contains more recursion level then allowed")]
    TooManyRecursionLevels,

    #[error(transparent)]
    MITInterfacesError(#[from] MITInterfacesError),
}
