mod errors;
mod export_it_functions;
mod export_it_records;
mod it_module_interface;

pub use errors::*;
pub use export_it_functions::*;
pub use export_it_records::*;
pub use it_module_interface::*;

pub type RIResult<T> = std::result::Result<T, ITInterfaceError>;

use mesh_it_interfaces::MITInterfaces;

/// Returns Mesh module interface that includes both export and all record types.
pub fn get_interface(mit: &MITInterfaces<'_>) -> RIResult<IModuleInterface> {
    let function_signatures = get_export_funcs(mit)?;
    let FullRecordTypes {
        record_types,
        export_record_types,
    } = get_record_types(mit, function_signatures.iter())?;

    let mm_interface = IModuleInterface {
        export_record_types,
        record_types,
        function_signatures,
    };

    Ok(mm_interface)
}
