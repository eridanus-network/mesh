#![warn(rust_2018_idioms)]
#![deny(
    dead_code,
    nonstandard_style,
    unused_imports,
    unused_mut,
    unused_variables,
    unused_unsafe,
    unreachable_patterns
)]

mod wasm_mem_pages_conversion;

pub use wasm_mem_pages_conversion::*;

use std::sync::Arc;

#[derive(Debug, Clone, PartialEq, Eq, Default, Hash)]
pub struct SharedString(pub Arc<String>);

impl std::borrow::Borrow<str> for SharedString {
    fn borrow(&self) -> &str {
        self.0.as_str()
    }
}
