mod caller;
mod function;
mod imports;
mod instance;
mod memory;
mod module;
mod store;
mod utils;
mod wasi;

use caller::*;
use function::*;
use imports::*;
use instance::*;
use memory::*;
use module::*;
use store::*;
use utils::*;
use wasi::*;

use mesh_wasm_backend_traits::prelude::*;

use wasmtime_wasi::WasiCtx;

#[derive(Clone, Default)]
pub struct WasmtimeWasmBackend {
    engine: wasmtime::Engine,
}

impl WasmBackend for WasmtimeWasmBackend {
    type Store = WasmtimeStore;
    type Module = WasmtimeModule;
    type Imports = WasmtimeImports;
    type Instance = WasmtimeInstance;
    type Context<'c> = WasmtimeContext<'c>;
    type ContextMut<'c> = WasmtimeContextMut<'c>;
    type ImportCallContext<'c> = WasmtimeImportCallContext<'c>;
    type HostFunction = WasmtimeFunction;
    type ExportFunction = WasmtimeFunction;
    type Memory = WasmtimeMemory;
    type MemoryView = WasmtimeMemory;
    type Wasi = WasmtimeWasi;

    fn new() -> WasmBackendResult<Self> {
        let mut config = wasmtime::Config::new();
        config
            .debug_info(false)
            .wasm_backtrace_details(wasmtime::WasmBacktraceDetails::Enable);
        let engine =
            wasmtime::Engine::new(&config).map_err(WasmBackendError::InitializationError)?;

        Ok(Self { engine })
    }
}

#[derive(Default)]
pub struct StoreState {
    wasi: Vec<WasiCtx>, // wasmtime store does not release memory until drop, so do we
}
