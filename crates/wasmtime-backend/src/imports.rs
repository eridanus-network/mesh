use crate::StoreState;
use crate::WasmtimeFunction;
use crate::WasmtimeStore;
use crate::WasmtimeWasmBackend;

use mesh_wasm_backend_traits::prelude::*;

#[derive(Clone)]
pub struct WasmtimeImports {
    pub(crate) linker: wasmtime::Linker<StoreState>,
}

impl Imports<WasmtimeWasmBackend> for WasmtimeImports {
    fn new(store: &mut WasmtimeStore) -> Self {
        Self {
            linker: wasmtime::Linker::new(store.inner.engine()),
        }
    }

    fn insert(
        &mut self,
        store: &impl AsContext<WasmtimeWasmBackend>,
        module: impl Into<String>,
        name: impl Into<String>,
        func: <WasmtimeWasmBackend as WasmBackend>::HostFunction,
    ) -> Result<(), ImportError> {
        let module = module.into();
        let name = name.into();
        self.linker
            .define(store.as_context(), &module, &name, func.inner)
            .map_err(|_| ImportError::DuplicateImport(module, name))
            .map(|_| ())
    }

    fn register<S, I>(
        &mut self,
        store: &impl AsContext<WasmtimeWasmBackend>,
        name: S,
        namespace: I,
    ) -> Result<(), ImportError>
    where
        S: Into<String>,
        I: IntoIterator<Item = (String, WasmtimeFunction)>,
    {
        let module: String = name.into();
        for (name, func) in namespace {
            self.insert(store, &module, name, func)?;
        }

        Ok(())
    }
}
