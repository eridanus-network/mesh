use super::ITGenerator;
use super::ITResolver;
use crate::Result;

use mesh_macro_impl::RecordType;
use mesh_macro_impl::RecordFields;

use wasmer_it::IRecordFieldType;
use wasmer_it::NEVec;

impl ITGenerator for RecordType {
    fn generate_it<'a>(&'a self, it_resolver: &mut ITResolver<'a>) -> Result<()> {
        let fields = match &self.fields {
            RecordFields::Named(fields) => fields,
            RecordFields::Unnamed(fields) => fields,
            RecordFields::Unit => return Ok(()),
        };

        let fields = fields
            .iter()
            .map(|field| IRecordFieldType {
                name: field.name.clone().unwrap_or_default(),
                ty: super::utils::ptype_to_itype_unchecked(&field.ty, it_resolver),
            })
            .collect::<Vec<_>>();

        let fields = NEVec::new(fields).map_err(|_| {
            crate::errors::ITGeneratorError::CorruptedRecord(format!(
                "serialized record with name '{}' contains no fields",
                self.name
            ))
        })?;

        it_resolver.add_record_type(self.name.clone(), fields);

        Ok(())
    }
}
