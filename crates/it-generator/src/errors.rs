use thiserror::Error as ThisError;
use serde_json::Error as SerdeDeserializationError;

#[derive(Debug, ThisError)]
pub enum ITGeneratorError {
    /// An error related to serde deserialization.
    #[error("Embedded by rust-sdk metadata couldn't be parsed by serde: {0:?}")]
    DeserializationError(#[from] SerdeDeserializationError),

    /// Various errors related to records
    #[error("{0}")]
    CorruptedRecord(String),

    /// Various errors occurred during the parsing/emitting a Wasm file.
    #[error("I/O error occurred: {0}")]
    IOError(String),
}
