#![warn(rust_2018_idioms)]
#![deny(
    dead_code,
    nonstandard_style,
    unused_imports,
    unused_mut,
    unused_variables,
    unused_unsafe,
    unreachable_patterns
)]

mod default_export_api_config;
mod errors;
mod instructions_generator;
mod interface_generator;

pub use interface_generator::embed_it;
pub use errors::ITGeneratorError;

pub const TYPE_RESOLVE_RECURSION_LIMIT: u32 = 1024;

pub(crate) type Result<T> = std::result::Result<T, ITGeneratorError>;
