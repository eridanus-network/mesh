use semver::Error as SemVerError;
use thiserror::Error as ThisError;
use std::str::Utf8Error;

#[derive(Debug, ThisError)]
pub enum SDKVersionError {
    /// Version can't be parsed to Utf8 string.
    #[error("embedded to the Wasm file version isn't valid UTF8 string: '{0}'")]
    VersionNotValidUtf8(Utf8Error),

    /// Version can't be parsed with semver.
    #[error("embedded to the Wasm file version is corrupted: '{0}'")]
    VersionCorrupted(#[from] SemVerError),
}
