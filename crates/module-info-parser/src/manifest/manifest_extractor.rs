use super::ModuleManifest;
use crate::ModuleInfoResult;
use crate::ModuleInfoError;
use crate::extract_custom_sections_by_name;
use crate::try_as_one_section;

use mesh_wasm_backend_traits::Module as ModuleTrait;
use mesh_wasm_backend_traits::WasmBackend;

use mesh_rs_sdk_main::MANIFEST_SECTION_NAME;
use walrus::ModuleConfig;
use walrus::Module;

use std::borrow::Cow;
use std::path::Path;
use std::convert::TryInto;

pub fn extract_from_path<P>(wasm_module_path: P) -> ModuleInfoResult<ModuleManifest>
where
    P: AsRef<Path>,
{
    let module = ModuleConfig::new()
        .parse_file(wasm_module_path)
        .map_err(ModuleInfoError::CorruptedWasmFile)?;

    extract_from_module(&module)
}

pub fn extract_from_module(wasm_module: &Module) -> ModuleInfoResult<ModuleManifest> {
    let sections = extract_custom_sections_by_name(wasm_module, MANIFEST_SECTION_NAME)?;
    let section = try_as_one_section(&sections, MANIFEST_SECTION_NAME)?;

    let manifest = match section {
        Cow::Borrowed(bytes) => (*bytes).try_into(),
        Cow::Owned(vec) => vec.as_slice().try_into(),
    }?;

    Ok(manifest)
}

pub fn extract_from_compiled_module<WB: WasmBackend>(
    module: &<WB as WasmBackend>::Module,
) -> ModuleInfoResult<ModuleManifest> {
    let sections = module.custom_sections(MANIFEST_SECTION_NAME);
    let section = try_as_one_section(sections, MANIFEST_SECTION_NAME)?;
    let manifest = section.as_slice().try_into()?;

    Ok(manifest)
}
