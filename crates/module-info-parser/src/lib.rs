#![warn(rust_2018_idioms)]
#![deny(
    dead_code,
    nonstandard_style,
    unused_imports,
    unused_mut,
    unused_variables,
    unused_unsafe,
    unreachable_patterns
)]

pub mod manifest;
pub mod sdk_version;
mod custom_section_extractor;
mod errors;

pub use errors::ModuleInfoError;

pub(crate) use custom_section_extractor::extract_custom_sections_by_name;
pub(crate) use custom_section_extractor::try_as_one_section;

pub(crate) type ModuleInfoResult<T> = std::result::Result<T, ModuleInfoError>;
