use walrus::CustomSection;
use walrus::IdsToIndices;

use std::borrow::Cow;

pub const IT_SECTION_NAME: &str = "interface-types";

#[derive(Debug, Clone)]
pub(super) struct ITCustomSection(pub Vec<u8>);

impl CustomSection for ITCustomSection {
    fn name(&self) -> &str {
        IT_SECTION_NAME
    }

    fn data(&self, _ids_to_indices: &IdsToIndices) -> Cow<'_, [u8]> {
        Cow::Borrowed(&self.0)
    }
}
