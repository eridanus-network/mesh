use super::custom::ITCustomSection;
use super::errors::ITParserError;
use crate::ParserResult;

use walrus::ModuleConfig;
use wasmer_it::ast::Interfaces;
use wasmer_it::decoders::wat::parse;
use wasmer_it::decoders::wat::Buffer;
use wasmer_it::ToBytes;

use std::path::Path;

/// Embed provided IT to a Wasm file by path.
pub fn embed_text_it<I, O>(in_wasm_path: I, out_wasm_path: O, it: &str) -> ParserResult<()>
where
    I: AsRef<Path>,
    O: AsRef<Path>,
{
    let module = ModuleConfig::new()
        .parse_file(in_wasm_path)
        .map_err(ITParserError::CorruptedWasmFile)?;

    let buffer = Buffer::new(it)?;
    let ast = parse(&buffer)?;

    let mut module = embed_it(module, &ast);
    module
        .emit_wasm_file(out_wasm_path)
        .map_err(ITParserError::WasmEmitError)?;

    Ok(())
}

/// Embed provided IT to a Wasm module.
pub fn embed_it(mut wasm_module: walrus::Module, interfaces: &Interfaces<'_>) -> walrus::Module {
    let mut bytes = vec![];
    // TODO: think about possible errors here
    interfaces.to_bytes(&mut bytes).unwrap();

    let custom = ITCustomSection(bytes);
    wasm_module.customs.add(custom);

    wasm_module
}
