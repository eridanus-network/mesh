#![warn(rust_2018_idioms)]
#![deny(
    dead_code,
    nonstandard_style,
    unused_imports,
    unused_mut,
    unused_variables,
    unused_unsafe,
    unreachable_patterns
)]

mod custom;
mod deleter;
mod embedder;
mod errors;
mod extractor;

pub use errors::ITParserError;

pub use deleter::delete_it_section;
pub use deleter::delete_it_section_from_file;

pub use embedder::embed_it;
pub use embedder::embed_text_it;

pub use extractor::extract_it_from_module;
pub use extractor::extract_text_it;
pub use extractor::extract_version_from_module;
pub use extractor::module_interface;
pub use extractor::module_it_interface;

pub mod interface {
    pub use mesh_module_interface::interface::FunctionSignature;
    pub use mesh_module_interface::interface::ModuleInterface;
    pub use mesh_module_interface::interface::RecordField;
    pub use mesh_module_interface::interface::RecordType;
}

pub mod it_interface {
    pub use mesh_module_interface::it_interface::IFunctionSignature;
    pub use mesh_module_interface::it_interface::IModuleInterface;
    pub use mesh_module_interface::it_interface::IRecordTypes;

    pub mod it {
        pub use wasmer_it::ast::FunctionArg as IFunctionArg;
        pub use wasmer_it::IRecordFieldType;
        pub use wasmer_it::IRecordType;
        pub use wasmer_it::IType;
    }
}

pub(crate) type ParserResult<T> = std::result::Result<T, ITParserError>;
