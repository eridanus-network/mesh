use crate::WasiError;
use crate::WasmBackend;

use std::path::PathBuf;
use std::collections::HashMap;
use std::collections::HashSet;

/// A type that provides WASI functionality to the given Wasm backend.
pub trait WasiImplementation<WB: WasmBackend> {
    /// Configures WASI state and adds WASI functions to the `imports` object.
    /// # Errors:
    ///     Returns an error if failed to open a preopen directory/file.
    fn register_in_linker(
        store: &mut <WB as WasmBackend>::ContextMut<'_>,
        linker: &mut <WB as WasmBackend>::Imports,
        config: WasiParameters,
    ) -> Result<(), WasiError>;

    /// Optional API for getting current WASI state.
    /// Returns None if not supported by current backend.
    fn get_wasi_state<'s>(
        instance: &'s mut <WB as WasmBackend>::Instance,
    ) -> Box<dyn WasiState + 's>;
}

#[derive(Default)]
pub struct WasiParameters {
    pub args: Vec<String>,
    pub envs: HashMap<String, String>,
    pub preopened_files: HashSet<PathBuf>,
    pub mapped_dirs: HashMap<String, PathBuf>,
}

pub trait WasiState {
    fn envs(&self) -> &[Vec<u8>];
}
