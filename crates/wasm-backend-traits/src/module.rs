use crate::ModuleCreationResult;
use crate::InstantiationResult;
use crate::WasmBackend;

/// A handle to compiled wasm module.
pub trait Module<WB: WasmBackend>: Sized {
    /// Compiles a wasm bytes into a module and extracts custom sections.
    fn new(store: &mut <WB as WasmBackend>::Store, wasm: &[u8]) -> ModuleCreationResult<Self>;

    /// Returns custom sections corresponding to `name`, empty slice if there is no sections.
    fn custom_sections(&self, name: &str) -> &[Vec<u8>];

    /// Instantiates module by allocating memory, VM state and linking imports with ones from `import` argument.
    /// Does not call `_start` or `_initialize` functions.
    ///
    /// # Panics:
    ///
    ///     If the `Store` given is not the same with `Store` used to create `Imports` and this object.
    fn instantiate(
        &self,
        store: &mut <WB as WasmBackend>::Store,
        imports: &<WB as WasmBackend>::Imports,
    ) -> InstantiationResult<<WB as WasmBackend>::Instance>;
}
