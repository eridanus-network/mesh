use crate::config::MeshConfig;
use crate::mesh_interface::MeshInterface;
use crate::MeshError;
use crate::MeshResult;
use crate::IValue;
use crate::IType;
use crate::MemoryStats;
use crate::module_loading::load_modules_from_fs;
use crate::host_imports::logger::LoggerFilter;
use crate::host_imports::logger::WASM_LOG_ENV_NAME;
use crate::json_to_mesh_err;

use mesh_wasm_backend_traits::WasmBackend;
#[cfg(feature = "raw-module-api")]
use mesh_wasm_backend_traits::WasiState;

use mesh_core::generic::MeshCore;
use mesh_core::IFunctionArg;
use mesh_core::MRecordTypes;
use mesh_utils::SharedString;
use mesh_rs_sdk::CallParameters;

use parking_lot::Mutex;
use serde_json::Value as JValue;

use std::convert::TryInto;
use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::Arc;

type MFunctionSignature = (Arc<Vec<IFunctionArg>>, Arc<Vec<IType>>);
type MModuleInterface = (Arc<Vec<IFunctionArg>>, Arc<Vec<IType>>, Arc<MRecordTypes>);

struct ModuleInterface {
    function_signatures: HashMap<SharedString, MFunctionSignature>,
    record_types: Arc<MRecordTypes>,
}

pub struct Mesh<WB: WasmBackend> {
    /// Mesh instance.
    core: MeshCore<WB>,

    /// Parameters of call accessible by Wasm modules.
    call_parameters: Arc<Mutex<CallParameters>>,

    /// Cached module interfaces by names.
    module_interfaces_cache: HashMap<String, ModuleInterface>,
}

impl<WB: WasmBackend> Mesh<WB> {
    /// Creates Mesh from config deserialized from TOML.
    pub fn with_raw_config<C>(config: C) -> MeshResult<Self>
    where
        C: TryInto<MeshConfig<WB>>,
        MeshError: From<C::Error>,
    {
        let config = config.try_into()?;
        let modules = config
            .modules_config
            .iter()
            .map(|m| -> MeshResult<(String, PathBuf)> {
                Ok((m.import_name.clone(), m.get_path(&config.modules_dir)?))
            })
            .collect::<MeshResult<HashMap<String, PathBuf>>>()?;

        Self::with_module_names::<MeshConfig<WB>>(&modules, config)
    }

    /// Creates Mesh with given modules.
    pub fn with_modules<C>(mut modules: HashMap<String, Vec<u8>>, config: C) -> MeshResult<Self>
    where
        C: TryInto<MeshConfig<WB>>,
        MeshError: From<C::Error>,
    {
        let mut mesh = MeshCore::new()?;
        let config = config.try_into()?;
        let call_parameters = Arc::<Mutex<CallParameters>>::default();

        let modules_dir = config.modules_dir;

        // LoggerFilter can be initialized with an empty string
        let wasm_log_env = std::env::var(WASM_LOG_ENV_NAME).unwrap_or_default();
        let logger_filter = LoggerFilter::from_env_string(&wasm_log_env);

        for module in config.modules_config {
            let module_bytes = modules.remove(&module.import_name).ok_or_else(|| {
                MeshError::InstantiationError {
                    module_import_name: module.import_name.clone(),
                    modules_dir: modules_dir.clone(),
                    provided_modules: modules.keys().cloned().collect::<Vec<_>>(),
                }
            })?;

            let mesh_module_config = crate::config::make_mesh_config(
                module.import_name.clone(),
                Some(module.config),
                call_parameters.clone(),
                &logger_filter,
            )?;
            mesh.load_module(module.import_name, &module_bytes, mesh_module_config)?;
        }

        Ok(Self {
            core: mesh,
            call_parameters,
            module_interfaces_cache: HashMap::new(),
        })
    }

    /// Searches for modules in `config.modules_dir`, loads only those in the `names` set
    pub fn with_module_names<C>(names: &HashMap<String, PathBuf>, config: C) -> MeshResult<Self>
    where
        C: TryInto<MeshConfig<WB>>,
        MeshError: From<C::Error>,
    {
        let config = config.try_into()?;
        let modules = load_modules_from_fs(names)?;

        Self::with_modules::<MeshConfig<WB>>(modules, config)
    }

    /// Call a specified function of loaded on a startup module by its name.
    pub fn call_with_ivalues(
        &mut self,
        module_name: impl AsRef<str>,
        func_name: impl AsRef<str>,
        args: &[IValue],
        call_parameters: mesh_rs_sdk::CallParameters,
    ) -> MeshResult<Vec<IValue>> {
        {
            // a separate code block to unlock the mutex ASAP and to avoid double locking
            let mut cp = self.call_parameters.lock();
            *cp = call_parameters;
        }

        self.core
            .call(module_name, func_name, args)
            .map_err(Into::into)
    }

    /// Call a specified function of loaded on a startup module by its name.
    pub fn call_with_json(
        &mut self,
        module_name: impl AsRef<str>,
        func_name: impl AsRef<str>,
        json_args: JValue,
        call_parameters: mesh_rs_sdk::CallParameters,
    ) -> MeshResult<JValue> {
        use it_json_serde::json_to_ivalues;
        use it_json_serde::ivalues_to_json;

        let module_name = module_name.as_ref();
        let func_name = func_name.as_ref();

        let (func_signature, output_types, record_types) =
            self.lookup_module_interface(module_name, func_name)?;
        let iargs = json_to_mesh_err!(
            json_to_ivalues(
                json_args,
                func_signature.iter().map(|arg| (&arg.name, &arg.ty)),
                &record_types,
            ),
            module_name.to_string(),
            func_name.to_string()
        )?;

        {
            // a separate code block to unlock the mutex ASAP and to avoid double locking
            let mut cp = self.call_parameters.lock();
            *cp = call_parameters;
        }

        let result = self.core.call(module_name, func_name, &iargs)?;

        json_to_mesh_err!(
            ivalues_to_json(result, &output_types, &record_types),
            module_name.to_string(),
            func_name.to_string()
        )
    }

    /// Return all export functions (name and signatures) of loaded modules.
    pub fn get_interface(&self) -> MeshInterface<'_> {
        let modules = self.core.interface().collect();

        MeshInterface { modules }
    }

    /// Return statistic of Wasm modules heap footprint.
    pub fn module_memory_stats(&self) -> MemoryStats<'_> {
        self.core.module_memory_stats()
    }

    /// At first, tries to find function signature and record types in module_interface_cache,
    /// if there is no them, tries to look
    fn lookup_module_interface(
        &mut self,
        module_name: &str,
        func_name: &str,
    ) -> MeshResult<MModuleInterface> {
        use MeshError::NoSuchModule;
        use MeshError::MissingFunctionError;

        if let Some(module_interface) = self.module_interfaces_cache.get(module_name) {
            if let Some(function) = module_interface.function_signatures.get(func_name) {
                return Ok((
                    function.0.clone(),
                    function.1.clone(),
                    module_interface.record_types.clone(),
                ));
            }

            return Err(MissingFunctionError(func_name.to_string()));
        }

        let module_interface = self
            .core
            .module_interface(module_name)
            .ok_or_else(|| NoSuchModule(module_name.to_string()))?;

        let function_signatures = module_interface
            .function_signatures
            .iter()
            .cloned()
            .map(|f| (SharedString(f.name), (f.arguments, f.outputs)))
            .collect::<HashMap<_, _>>();

        let (arg_types, output_types) = function_signatures
            .get(func_name)
            .ok_or_else(|| MissingFunctionError(func_name.to_string()))?;

        let arg_types = arg_types.clone();
        let output_types = output_types.clone();
        let record_types = Arc::new(module_interface.record_types.clone());

        let module_interface = ModuleInterface {
            function_signatures,
            record_types: record_types.clone(),
        };

        self.module_interfaces_cache
            .insert(func_name.to_string(), module_interface);

        Ok((arg_types, output_types, record_types))
    }
}

// This API is intended for testing purposes (mostly in Mesh REPL)
#[cfg(feature = "raw-module-api")]
impl<WB: WasmBackend> Mesh<WB> {
    pub fn load_module<C, S>(
        &mut self,
        name: S,
        wasm_bytes: &[u8],
        config: Option<C>,
    ) -> MeshResult<()>
    where
        S: Into<String>,
        C: TryInto<crate::generic::MeshModuleConfig<WB>>,
        MeshError: From<C::Error>,
    {
        let config = config.map(|c| c.try_into()).transpose()?;
        let name = name.into();

        // LoggerFilter can be initialized with an empty string
        let wasm_log_env = std::env::var(WASM_LOG_ENV_NAME).unwrap_or_default();
        let logger_filter = LoggerFilter::from_env_string(&wasm_log_env);

        let mesh_module_config = crate::config::make_mesh_config(
            name.clone(),
            config,
            self.call_parameters.clone(),
            &logger_filter,
        )?;
        self.core
            .load_module(name, wasm_bytes, mesh_module_config)
            .map_err(Into::into)
    }

    pub fn unload_module(&mut self, module_name: impl AsRef<str>) -> MeshResult<()> {
        self.core.unload_module(module_name).map_err(Into::into)
    }

    pub fn module_wasi_state(
        &mut self,
        module_name: impl AsRef<str>,
    ) -> MeshResult<Box<dyn WasiState + '_>> {
        let module_name = module_name.as_ref();

        self.core
            .module_wasi_state(module_name)
            .ok_or_else(|| MeshError::NoSuchModule(module_name.to_string()))
    }
}
