#![warn(rust_2018_idioms)]
#![deny(
    dead_code,
    nonstandard_style,
    unused_imports,
    unused_mut,
    unused_variables,
    unused_unsafe,
    unreachable_patterns
)]

mod config;
mod host_imports;
mod errors;
mod mesh;
mod mesh_interface;
mod module_loading;

pub(crate) type MeshResult<T> = std::result::Result<T, MeshError>;

pub use mesh_interface::MeshInterface;

pub use config::ConfigContext;
pub use config::WithContext;
pub use config::MeshWASIConfig;

pub use config::TomlMeshConfig;
pub use config::TomlMeshModuleConfig;
pub use config::TomlMeshNamedModuleConfig;
pub use config::TomlWASIConfig;

pub use errors::MeshError;

// Re-exports from Mesh
pub use mesh_core::IValue;
pub use mesh_core::IRecordType;
pub use mesh_core::IFunctionArg;
pub use mesh_core::IType;
pub use mesh_core::MModuleInterface as MeshModuleInterface;
pub use mesh_core::MFunctionSignature as MeshFunctionSignature;
pub use mesh_core::MemoryStats;
pub use mesh_core::ModuleMemoryStat;
pub use mesh_core::MRecordTypes;
pub use mesh_core::HostImportError;
pub use mesh_core::to_interface_value;
pub use mesh_core::from_interface_values;
pub use mesh_core::ne_vec;

pub use mesh_module_interface::interface::itype_text_view;

pub use mesh_rs_sdk::CallParameters;
pub use mesh_rs_sdk::SecurityTetraplet;

pub mod generic {
    pub use crate::mesh::Mesh;
    pub use crate::config::MeshModuleConfig;
    pub use crate::config::ModuleDescriptor;
    pub use crate::config::MeshConfig;

    pub use mesh_core::generic::*;
}

#[cfg(feature = "default")]
pub mod wasmtime {
    pub type WasmBackend = mesh_core::wasmtime::WasmBackend;

    pub type Mesh = crate::mesh::Mesh<WasmBackend>;
    pub type MeshModuleConfig = crate::config::MeshModuleConfig<WasmBackend>;
    pub type ModuleDescriptor = crate::config::ModuleDescriptor<WasmBackend>;
    pub type MeshConfig = crate::config::MeshConfig<WasmBackend>;

    pub use mesh_core::wasmtime::HostExportedFunc;
    pub use mesh_core::wasmtime::HostImportDescriptor;
}

#[cfg(feature = "default")]
pub use wasmtime::*;
