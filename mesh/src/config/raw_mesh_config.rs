use crate::MeshError;
use crate::MeshResult;

use serde_derive::Serialize;
use serde_derive::Deserialize;
use serde_with::serde_as;
use serde_with::skip_serializing_none;
use serde_with::DisplayFromStr;

use std::path::Path;
use std::path::PathBuf;

#[derive(Deserialize, Serialize, Debug, Clone, Default)]
pub struct TomlMeshConfig {
    pub modules_dir: Option<PathBuf>,
    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub module: Vec<TomlMeshNamedModuleConfig>,
    pub default: Option<TomlMeshModuleConfig>,
    #[serde(skip)]
    pub base_path: PathBuf,
}

impl TomlMeshConfig {
    /// Load config from filesystem.
    pub fn load<P: AsRef<Path>>(path: P) -> MeshResult<Self> {
        let path = PathBuf::from(path.as_ref()).canonicalize().map_err(|e| {
            MeshError::IOError(format!(
                "failed to canonicalize path {}: {}",
                path.as_ref().display(),
                e
            ))
        })?;

        let file_content = std::fs::read(&path).map_err(|e| {
            MeshError::IOError(format!("failed to load {}: {}", path.display(), e))
        })?;

        let mut config: TomlMeshConfig = toml::from_slice(&file_content)?;

        let default_base_path = Path::new("/");
        config.base_path = path
            .canonicalize()
            .map_err(|e| {
                MeshError::IOError(format!(
                    "Failed to canonicalize config path {}: {}",
                    path.display(),
                    e
                ))
            })?
            .parent()
            .unwrap_or(default_base_path)
            .to_path_buf();

        Ok(config)
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, Default)]
pub struct TomlMeshNamedModuleConfig {
    pub name: String,
    #[serde(default)]
    pub load_from: Option<PathBuf>,
    #[serde(default)]
    pub file_name: Option<String>,
    #[serde(flatten)]
    pub config: TomlMeshModuleConfig,
}

#[serde_as]
#[skip_serializing_none]
#[derive(Deserialize, Serialize, Debug, Clone, Default)]
pub struct TomlMeshModuleConfig {
    pub mem_pages_count: Option<u32>,
    #[serde_as(as = "Option<DisplayFromStr>")]
    #[serde(default)]
    pub max_heap_size: Option<bytesize::ByteSize>,
    pub logger_enabled: Option<bool>,
    pub logging_mask: Option<i32>,
    pub wasi: Option<TomlWASIConfig>,
    pub mounted_binaries: Option<toml::value::Table>,
}

#[derive(Deserialize, Serialize, Debug, Clone, Default)]
pub struct TomlWASIConfig {
    pub preopened_files: Option<Vec<PathBuf>>,
    pub envs: Option<toml::value::Table>,
    pub mapped_dirs: Option<toml::value::Table>,
}
