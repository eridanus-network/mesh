use crate::MeshError;
use crate::MeshResult;

use std::path::Path;
use std::path::PathBuf;

pub fn as_relative_to_base(base: Option<&Path>, path: &Path) -> MeshResult<PathBuf> {
    if path.is_absolute() {
        return Ok(PathBuf::from(path));
    }

    let path = match base {
        None => PathBuf::from(path),
        Some(base) => base.join(path),
    };

    path.canonicalize().map_err(|e| {
        MeshError::IOError(format!(
            "Failed to canonicalize path {}: {}",
            path.display(),
            e
        ))
    })
}
