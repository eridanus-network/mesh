use crate::MeshError;
use crate::MeshResult;

use std::collections::HashMap;
use std::path::PathBuf;

/// Loads modules from a directory at a given path. Non-recursive, ignores subdirectories.
pub(crate) fn load_modules_from_fs(
    modules: &HashMap<String, PathBuf>,
) -> MeshResult<HashMap<String, Vec<u8>>> {
    let loaded = modules
        .iter()
        .try_fold(HashMap::new(), |mut hash_map, (import_name, path)| {
            let module_bytes = std::fs::read(path).map_err(|e| {
                MeshError::IOError(format!("failed to load {}: {}", path.display(), e))
            })?;

            if hash_map.insert(import_name.clone(), module_bytes).is_some() {
                return Err(MeshError::InvalidConfig(String::from(
                    "module {} is duplicated in config",
                )));
            }

            Ok(hash_map)
        })?;

    Ok(loaded)
}
