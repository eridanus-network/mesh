mod exports;
mod mesh_module;
mod wit_function;
mod wit_instance;
mod type_converters;

use mesh_wasm_backend_traits::WValue;

pub use wit_instance::MRecordTypes;

pub use wasmer_it::IType;
pub use wasmer_it::IRecordType;
pub use wasmer_it::ast::FunctionArg as IFunctionArg;
pub use wasmer_it::IValue;
pub use wasmer_it::from_interface_values;
pub use wasmer_it::to_interface_value;

use serde::Serialize;
use serde::Deserialize;
use std::sync::Arc;

/// Represent a function type inside Mesh module.
#[derive(PartialEq, Eq, Debug, Clone, Hash, Serialize, Deserialize)]
pub struct MFunctionSignature {
    pub name: Arc<String>,
    pub arguments: Arc<Vec<IFunctionArg>>,
    pub outputs: Arc<Vec<IType>>,
}

pub(crate) use mesh_module::MModule;

// types that often used together
pub(crate) mod wit_prelude {
    pub(super) use super::wit_instance::ITInstance;
    pub(super) use super::exports::ITExport;
    pub(super) use crate::MError;
    pub(super) use super::wit_function::WITFunction;
}
