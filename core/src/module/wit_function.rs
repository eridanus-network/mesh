use super::mesh_module::Callable;
use super::mesh_module::MModule;
use super::IFunctionArg;
use super::IType;
use super::IValue;
use crate::MResult;

use mesh_wasm_backend_traits::DelayedContextLifetime;
use mesh_wasm_backend_traits::ExportFunction;
use mesh_wasm_backend_traits::WValue;
use mesh_wasm_backend_traits::WasmBackend;

use wasmer_it::interpreter::wasm;

use std::sync::Arc;

#[derive(Clone)]
enum WITFunctionInner<WB: WasmBackend> {
    Export {
        func: Arc<<WB as WasmBackend>::ExportFunction>,
    },
    Import {
        // TODO: use dyn Callable here
        callable: Arc<Callable<WB>>,
    },
}

/// Represents all import and export functions that could be called from IT context by call-core.
#[derive(Clone)]
pub(super) struct WITFunction<WB: WasmBackend> {
    name: String,
    arguments: Arc<Vec<IFunctionArg>>,
    outputs: Arc<Vec<IType>>,
    inner: WITFunctionInner<WB>,
}

impl<WB: WasmBackend> WITFunction<WB> {
    /// Creates functions from a "usual" (not IT) module export.
    pub(super) fn from_export(
        store: &mut <WB as WasmBackend>::Store,
        dyn_func: <WB as WasmBackend>::ExportFunction,
        name: String,
    ) -> MResult<Self> {
        use super::type_converters::wtype_to_itype;
        let signature = dyn_func.signature(store);
        let arguments = signature
            .params()
            .iter()
            .map(|wtype| IFunctionArg {
                // here it's considered as an anonymous arguments
                name: String::new(),
                ty: wtype_to_itype(wtype),
            })
            .collect::<Vec<_>>();
        let outputs = signature
            .returns()
            .iter()
            .map(wtype_to_itype)
            .collect::<Vec<_>>();

        let inner = WITFunctionInner::Export {
            func: Arc::new(dyn_func),
        };

        let arguments = Arc::new(arguments);
        let outputs = Arc::new(outputs);

        Ok(Self {
            name,
            arguments,
            outputs,
            inner,
        })
    }

    /// Creates function from a module import.
    pub(super) fn from_import(
        wit_module: &MModule<WB>,
        module_name: &str,
        function_name: &str,
        arguments: Arc<Vec<IFunctionArg>>,
        outputs: Arc<Vec<IType>>,
    ) -> MResult<Self> {
        let callable = wit_module.get_callable(module_name, function_name)?;

        let inner = WITFunctionInner::Import { callable };

        let name = function_name.to_string();

        Ok(Self {
            name,
            arguments,
            outputs,
            inner,
        })
    }
}

impl<WB: WasmBackend> wasm::structures::LocalImport<DelayedContextLifetime<WB>>
    for WITFunction<WB>
{
    fn name(&self) -> &str {
        self.name.as_str()
    }

    fn inputs_cardinality(&self) -> usize {
        self.arguments.len()
    }

    fn outputs_cardinality(&self) -> usize {
        self.outputs.len()
    }

    fn arguments(&self) -> &[IFunctionArg] {
        &self.arguments
    }

    fn outputs(&self) -> &[IType] {
        &self.outputs
    }

    fn call(
        &self,
        store: &mut <WB as WasmBackend>::ContextMut<'_>,
        arguments: &[IValue],
    ) -> std::result::Result<Vec<IValue>, ()> {
        use super::type_converters::ival_to_wval;
        use super::type_converters::wval_to_ival;
        match &self.inner {
            WITFunctionInner::Export { func, .. } => func
                .as_ref()
                .call(
                    store,
                    arguments
                        .iter()
                        .map(ival_to_wval)
                        .collect::<Vec<WValue>>()
                        .as_slice(),
                )
                .map_err(|_| ())
                .map(|results| results.iter().map(wval_to_ival).collect()),
            WITFunctionInner::Import { callable, .. } => Arc::make_mut(&mut callable.clone())
                .call(store, arguments)
                .map_err(|_| ()),
        }
    }
}
