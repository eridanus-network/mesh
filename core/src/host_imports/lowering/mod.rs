/// Contain functions intended to put (lower) IValues to Wasm memory
/// and pass it to a Wasm module as raw WValues (Wasm types).
mod lo_helper;
mod lower_ivalues;

pub(crate) use lo_helper::LoHelper;
pub(crate) use lower_ivalues::ivalue_to_wvalues;

use super::WValue;
use super::AllocateFunc;
use super::HostImportResult;
