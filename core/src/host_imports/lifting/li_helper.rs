use crate::MRecordTypes;
use crate::IRecordType;
use it_lilo::traits::RecordResolvable;
use it_lilo::traits::RecordResolvableError;

use std::sync::Arc;

pub(crate) struct LiHelper {
    record_types: Arc<MRecordTypes>,
}

impl LiHelper {
    pub(crate) fn new(record_types: Arc<MRecordTypes>) -> Self {
        Self { record_types }
    }
}

impl RecordResolvable for LiHelper {
    fn resolve_record(&self, record_type_id: u64) -> Result<&IRecordType, RecordResolvableError> {
        self.record_types
            .get(&record_type_id)
            .map(|r| r.as_ref())
            .ok_or(RecordResolvableError::RecordNotFound(record_type_id))
    }
}
