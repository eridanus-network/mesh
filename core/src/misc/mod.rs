mod errors;
mod version_checker;

pub(crate) use errors::PrepareError;
pub(crate) use version_checker::check_sdk_version;
pub(crate) use version_checker::check_it_version;

type PrepareResult<T> = std::result::Result<T, PrepareError>;
