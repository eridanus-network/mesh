#![warn(rust_2018_idioms)]
#![feature(get_mut_unchecked)]
#![feature(new_uninit)]
#![feature(stmt_expr_attributes)]
#![deny(
    dead_code,
    nonstandard_style,
    unused_imports,
    unused_mut,
    unused_variables,
    unused_unsafe,
    unreachable_patterns
)]

mod config;
mod mesh_core;
mod errors;
mod host_imports;
mod misc;
mod module;
mod memory_statistic;

pub use crate::mesh_core::MModuleInterface;
pub use errors::MError;
pub use host_imports::HostImportError;
pub use module::IValue;
pub use module::IRecordType;
pub use module::IFunctionArg;
pub use module::IType;
pub use module::MRecordTypes;
pub use module::MFunctionSignature;
pub use module::from_interface_values;
pub use module::to_interface_value;
pub use memory_statistic::ModuleMemoryStat;
pub use memory_statistic::MemoryStats;

pub use wasmer_it::IRecordFieldType;
pub mod ne_vec {
    pub use wasmer_it::NEVec;
}

pub(crate) type MResult<T> = std::result::Result<T, MError>;

pub mod generic {
    pub use crate::config::MModuleConfig;
    pub use crate::config::HostExportedFunc;
    pub use crate::config::HostImportDescriptor;
    pub use crate::mesh_core::MeshCore;
}

#[cfg(feature = "default")]
pub mod wasmtime {
    pub type WasmBackend = mesh_wasmtime_backend::WasmtimeWasmBackend;

    pub type MModuleConfig = crate::config::MModuleConfig<WasmBackend>;
    pub type HostExportedFunc = crate::config::HostExportedFunc<WasmBackend>;
    pub type HostImportDescriptor = crate::config::HostImportDescriptor<WasmBackend>;
    pub type MeshCore = crate::mesh_core::MeshCore<WasmBackend>;
}

#[cfg(feature = "default")]
pub use crate::wasmtime::*;
