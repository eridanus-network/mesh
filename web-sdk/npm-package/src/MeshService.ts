
import { init } from './mesh_js.js';
import type {MeshServiceConfig, Env} from './config.js';
import {CallParameters, JSONArray, JSONObject, LogFunction} from './types.js';

type Awaited<T> = T extends PromiseLike<infer U> ? U : T;

type ControlModuleInstance = Awaited<ReturnType<typeof init>> | 'not-set' | 'terminated';

export class MeshService {
    private env: Env = {};

    private _controlModuleInstance: ControlModuleInstance = 'not-set';

    constructor(
        private readonly controlModule: WebAssembly.Module,
        private readonly serviceId: string,
        private logFunction: LogFunction,
        private serviceConfig: MeshServiceConfig,
        private modules: { [x: string]: Uint8Array },
        env?: Env,
    ) {
        this.serviceConfig.modules_config.forEach(module => {
            module.config.wasi.envs = {
                WASM_LOG: 'off',            // general default
                ...env,                     // overridden by global envs
                ...module.config.wasi.envs, // overridden by module-wise envs
            }
        })
    }

    async init(): Promise<void> {
        const controlModuleInstance = await init(this.controlModule);

        controlModuleInstance.register_module(this.serviceConfig, this.modules, this.logFunction);
        this._controlModuleInstance = controlModuleInstance;
    }

    terminate(): void {
        this._controlModuleInstance = 'not-set';
    }

    call(functionName: string, args: JSONArray | JSONObject, callParams: CallParameters): unknown {
        if (this._controlModuleInstance === 'not-set') {
            throw new Error('Not initialized');
        }

        if (this._controlModuleInstance === 'terminated') {
            throw new Error('Terminated');
        }

        // facade module is the last module of the service
        const facade_name = this.serviceConfig.modules_config[this.serviceConfig.modules_config.length - 1].import_name;
        const argsString = JSON.stringify(args);
        const rawRes = this._controlModuleInstance.call_module(facade_name, functionName, argsString, callParams);
        return JSON.parse(rawRes);
    }
}
