#! /bin/bash

wasm-pack build -d mesh-web-pkg --target web

MESH_WEB_JS_DEST=npm-package/src/snippets/mesh-web-6faa67b8af9cc173/
mkdir -p $MESH_WEB_JS_DEST
cp mesh-web.js $MESH_WEB_JS_DEST