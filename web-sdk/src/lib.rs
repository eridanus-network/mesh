mod api;
mod global_state;
mod logger;

use crate::logger::MeshLogger;

use wasm_bindgen::prelude::*;

#[wasm_bindgen(start)]
fn main() {
    log::set_boxed_logger(Box::new(MeshLogger::new(log::LevelFilter::Info))).unwrap();
    // Trace is required to accept all logs from a service.
    // Max level for this crate is set in MeshLogger constructor.
    log::set_max_level(log::LevelFilter::Trace);
}
