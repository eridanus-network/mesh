use mesh::generic::Mesh;
use mesh_js_backend::JsWasmBackend;

use std::cell::RefCell;

thread_local!(pub(crate) static MESH: RefCell<Option<Mesh<JsWasmBackend>>> = RefCell::new(None));
